<?php 	;?>
<div id="xrcbplayer-container">
	<div class="xrcbplayer">
		<div class="player playing-off">
			<content class="listening-info marquee">
				<span>
					<ul>
						<li id="live" class="live"></span>
							<li class="radio"></li>
							<li class="sep">-</li>
							<li class="programa"></li>
							<li class="podcast"></li>
						</ul>
					</span>
			</content>
			<audio id="xrcbplayer" type="audio/mpeg" controls="controls">
					<p>Your browser doesn\'t support HTML5 audio. Here is a <a href="">link to the audio</a> instead.</p>
			</audio>
			<div class="listento-switch">
				<input id="toggle-live" class="toggle toggle-live" name="toggle" value="true" type="radio" checked>
				<label for="toggle-live" class="btn toggle-live"  title="escucha en directo">on air</label>
				<input id="toggle-podcast" class="toggle toggle-podcast" name="toggle" value="false" type="radio" disabled>
				<label for="toggle-podcast" class="btn toggle-podcast"  title="escucha un podcast">podcast</label>
			</div>
			<nav class="moreinfo">
				<a href="#"><span class="hide">+ info</span></a>
			</nav>
			<aside class="moreinfo-aside">
				<div class="moreinfo-content">
				</div>
			</aside>
			<!-- <control class="volume"></control> -->
		</div>
	</div>
</div>


<script type="text/javascript">
	var player = null;
</script>
<?php ;?>
