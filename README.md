# XRCB Player Wordpress Plugin

Wordpress mp3 player plugin for https://xrcb.cat/

## Insert player in Wordpress Template

Use the following function to insert the XRCB player:

```
if( function_exists( 'xrcbplayer_show_player' ) ) {
   	xrcbplayer_show_player();
}
```
