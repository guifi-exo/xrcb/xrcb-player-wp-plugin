var broadcastUrl = "https://icecast.xrcb.cat/main.mp3",
	liveUrl = "https://icecast.xrcb.cat/main.mp3",
	isPlaying = false,
	pullTitleTimer = null;

jQuery(document).ready(function(jQuery) {

	// get parameters
	var url = getUrlParameter('url');
	var radio = getUrlParameter('radio');
	var programa = getUrlParameter('programa');
	var podcast = getUrlParameter('podcast');
	var radiolink = getUrlParameter('radio-link');
	var podcastlink = getUrlParameter('podcast-link');

	jQuery('#xrcbplayer').mediaelementplayer({
		alwaysShowControls: true,
		features: ['playpause','volume','current','progress','duration'],
		//stretching: 'responsive',
		audioWidth: '100%',
		audioHeight: 60,
		audioVolume: 'horizontal',
		hideVolumeOnTouchDevices: false,
		success: function(mediaElement, originalNode, instance) {
			console.log(mediaElement.id, "loaded");
			player = jQuery("#"+mediaElement.id)[0];

			// Event listener
			mediaElement.addEventListener( 'playing', function( e ) {
				isPlaying = true;
				// if playing broadcast, pull title of playing audio every 60 seconds
				if (pullTitleTimer)
				clearInterval(pullTitleTimer);
				if (player.getSrc() == broadcastUrl || player.getSrc() == liveUrl)
				pullTitleTimer = setInterval(setStreamInfo, 60000);
			}, false);
			mediaElement.addEventListener( 'pause', function( e ) {
				isPlaying = false;
			}, false);
			mediaElement.addEventListener( 'ended', function( e ) {
				isPlaying = false;
			}, false);

			// set initial stream info
			setStreamInfo();

			jQuery(".xrcbplayer .moreinfo-content .close").click(function() {
				jQuery(".xrcbplayer .moreinfo-content").slideUp();
				jQuery(".xrcbplayer .moreinfo-content").css("pointer-events", "none");
			});

			jQuery("input[name='toggle']").change(function() {
				if (jQuery("#toggle-live").is(':checked')) {
					playBroadcast();
					jQuery(".xrcbplayer .player").removeClass("playing-off");
					jQuery(".xrcbplayer .player").addClass("playing-on");
					jQuery(".xrcbplayer .player").removeClass("src-podcast");
					jQuery(".xrcbplayer .player").addClass("src-stream");
				}
			});

			jQuery("label.toggle-podcast").click(function() {
				jQuery(".podcast-btn a").click();
			});

			mediaElement.addEventListener('loadeddata', function() {
				// listening to playing event
				if (player.getSrc() == broadcastUrl || player.getSrc() == liveUrl) {
					jQuery(".broadcast-btn").addClass("live");
					jQuery(".xrcbplayer .player").removeClass("playing-off");
					jQuery(".xrcbplayer .player").addClass("playing-on");
					jQuery(".xrcbplayer .player").removeClass("src-podcast");// remove class playing podcast
					jQuery(".xrcbplayer .player").addClass("src-stream");// set class playing stream
				}
				else {
					jQuery(".broadcast-btn").removeClass("live");
				}
			}, false);
		}
	});

	bindPlayer();
});

var getUrlParameter = function getUrlParameter(sParam) {
	var sPageURL = decodeURIComponent(window.location.search.substring(1)),
	sURLVariables = sPageURL.split('&'),
	sParameterName,
	i;

	for (i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split('=');

		if (sParameterName[0] === sParam) {
			return sParameterName[1] === undefined ? true : sParameterName[1];
		}
	}
};

function bindPlayer() {
	jQuery('.btn-play').unbind();
	jQuery('.btn-play').click(function() {

		if (!jQuery(this).hasClass("active")) {
			jQuery(".xrcbplayer .player").removeClass("playing-off");
			jQuery(".xrcbplayer .player").addClass("playing-on");

			xrcbplay(jQuery(this).data("src"));

			if (jQuery(this).data("src") == broadcastUrl || jQuery(this).data("src") == liveUrl) {
				// stream
				jQuery(".xrcbplayer #live").addClass("live");
				jQuery(".broadcast-btn").addClass("live");
				jQuery(".xrcbplayer .sep").css("display", "none");
			}
			else {
				// podcast
				jQuery(".xrcbplayer #live").removeClass("live");
				jQuery(".broadcast-btn").removeClass("live");
				jQuery(".xrcbplayer .sep").css("display", "inline-block");

				setPodcastInfo(jQuery(this));
			}
			jQuery(".xrcbplayer #live").text("");
			jQuery(".xrcbplayer .radio").text(jQuery(this).data("radio"));

			if (jQuery(this).data("programa") && jQuery(this).data("programa") !== "")
				jQuery(".xrcbplayer .programa").text("[" + jQuery(this).data("programa") + "] ");
			else
				jQuery(".xrcbplayer .programa").text("");
			jQuery(".xrcbplayer .podcast").html("<a href='"+jQuery(this).data("podcast-link")+"'>"+jQuery(this).data("title")+"</a>");
			jQuery(".xrcbplayer .podcast-link").text(jQuery(this).data("podcast-link"));
			
			player.play();
			jQuery("#toggle-podcast").prop('checked', true);
  			jQuery(".xrcbplayer .player").removeClass("src-stream");// remove class playing stream
			jQuery(".xrcbplayer .player").addClass("src-podcast");// set class playing podcast
			jQuery(".btn-play").removeClass("active");
			jQuery(this).addClass("active");

			// set row of datatable active
			jQuery(".btn-play").closest("tr").removeClass("active");
			jQuery(this).closest("tr").addClass("active");

			// mobile bg player state
			jQuery("#mobileplayer .play").addClass("pause");

			// track podcast plays
			_paq.push(['trackLink', jQuery(this).data("src"), 'download', {dimension1: jQuery(this).data("title") } ]);
		}
		else {
			player.pause();
			jQuery(this).removeClass("active");
			jQuery(this).closest("tr").removeClass("active");

			// mobile bg player state
			jQuery("#mobileplayer .play").removeClass("pause");
		}
	});

	jQuery(".xrcbplayer .mejs-play button").click(function() {
		if (jQuery(this).attr("title") == "Reprodueix" ||
			jQuery(this).attr("title") == "Reproducir" ||
			jQuery(this).attr("title") == "Play") {

			// load live player source for default player status on page loading
			if (player.getSrc() == null) {
				// play broadcast
				xrcbplay(broadcastUrl);
				jQuery(".xrcbplayer #live").addClass("live");
				jQuery(".xrcbplayer .player").removeClass("playing-off");
				jQuery(".xrcbplayer .player").addClass("playing-on");
				jQuery(".xrcbplayer .player").removeClass("src-podcast");// remove class playing podcast
				jQuery(".xrcbplayer .player").addClass("src-stream");// set class playing stream
				setStreamInfo();
			}
			else if (player.getSrc() == broadcastUrl || player.getSrc() == liveUrl) {
				// play broadcast
				jQuery(".xrcbplayer .player").removeClass("playing-off");
				jQuery(".xrcbplayer .player").addClass("playing-on");
				jQuery(".xrcbplayer .player").removeClass("src-podcast");// remove class playing podcast
				jQuery(".xrcbplayer .player").addClass("src-stream");// set class playing stream
				setStreamInfo();
			}
			else {
				// play podcast
	  			jQuery(".xrcbplayer .player").removeClass("src-stream");
				jQuery(".xrcbplayer .player").addClass("src-podcast");
			}

			// mobile bg player state
			jQuery("#mobileplayer .play").addClass("pause");
		}
		else {
			// mobile bg player state
			jQuery("#mobileplayer .play").removeClass("pause");
		}
	});
}

function xrcbplay(url) {
	// no cache: https://github.com/mediaelement/mediaelement/issues/1321
	//var nocache = Math.floor((Math.random() * 1000000) + 1);
	//player.setSrc(url+'?nocache='+nocache);
	player.setSrc(url);
}

function setStreamInfo() {
	//var nocache = Math.floor((Math.random() * 1000000) + 1);
	jQuery.when(
		//jQuery.getJSON("/wp-json/xrcb/v1/broadcast?nocache="+nocache, function( audio ) {})
		jQuery.getJSON("/wp-json/xrcb/v1/broadcast", function( audio ) {})
	).then(function(audio) {

		var info = "<p><strong>emissió XRCB</strong></p>";

		if (audio.data) {

			//info += "<a class='radioton' href='https://xrcb.cat/ca/event/radioton-02-radio-cava-ret/'><img src='https://xrcb.cat/wp-content/uploads/2019/06/Oreja_radioTon2_small.gif' /></a>"; // show radioton logo
			info += "<div class='infoTitle'>";

			if (audio.data.live) {
				info += audio.data.title;
				// live -> title from calendar
				//jQuery(".xrcbplayer #live").text(": "+audio.data.radio_name+" - "+audio.data.title);
				jQuery(".xrcbplayer #live").text(" - "+shortenStr("",audio.data.title));

				info += "</div>";
			}
			else {
				info +="<a href='"+audio.data.link+"'>"+audio.data.title+"</a>";
				// broadcast -> title from icecast
				setStreamTitle(audio.data.title);

				info += "</div>";
				info += "<div class='infoSubtitle'><strong><a href='"+audio.data.radio_link+"'>"+audio.data.radio_name+"</a></strong></div>";
			}

			var desc = audio.data.description;
			if (desc) {
				if (desc.length > 150) {
					desc = desc.substring(0,150)+"...";
				}
				info += "<p>"+desc.replace(/<\/?[^>]+(>|$)/g, "")+"</p></br>";
			}
		}

		var src = "https://player.xrcb.cat/?url="+encodeURIComponent(broadcastUrl)+"&podcast=Emissió XRCB";

		//let siteUrl = window.location.protocol.toString() + "//" + window.location.host.toString();
		info += '<a download="" href="https://icecast.xrcb.cat/main.mp3.m3u" class="icon"><i class="fa fa-15x fa-external-link-square btn-mp3" aria-hidden="true"></i></a> ';
		info += '<i class="fa fa-15x fa-code btn-embed" aria-hidden="true"></i> ';
		info += '<i class="fa fa-15x fa-twitter btn-twitter" aria-hidden="true"></i></div>';
		info += "<div class='embed_code'><pre>&lt;iframe width='600' height='60' src='"+src+"' frameborder='0' scrolling='no' style='overflow: hidden; max-width: 100%;'&gt;Iframe no esta soportado en tu navegador.&lt;/iframe&gt;</pre></div>";

		jQuery(".xrcbplayer .moreinfo-content").empty();
		jQuery(".xrcbplayer .moreinfo-content").append(info);

		bindInfoBtns();
	});
}

function setStreamTitle(standbyTitle="") {

	if (true) {
		// metainfo de liquidsoup
		jQuery.ajax({
			url: "https://xrcb.cat/mounts.xsl",
			success: function(data) {
				//console.log(data);
				var title = data.mounts["/main.mp3"].title;
				if (title === "" || title.toLowerCase().indexOf("unknown") !== -1) {
					title = standbyTitle;
				}
				jQuery(".xrcbplayer #live").text(shortenStr(""," - "+title));
			}
		});
	}
	else {
		// manually setting of streaming label
		jQuery(".xrcbplayer #live").text(shortenStr(""," - Magazine Cultural Ràdio Rambles. Inauguración de la nueva estación de radio."));
	}
}

function setPodcastInfo(btn) {
	var info = "<div class='infoTitle'><a href='"+btn.data("podcast-link")+"'>"+btn.data("title")+"</a></div>";
	info += "<div class='infoSubtitle'><a href='"+btn.data("radio-link")+"'>"+btn.data("radio")+"</a></div>";
	if (btn.data("programa") && btn.data("programa") != "") {
		info += "<div class='infoProgram'>"+btn.data("programa")+"</div>";
	}

	info += '<div class="icons"><a href="'+btn.data("src")+'" download class="icon"><i class="fa fa-15x fa-arrow-down" aria-hidden="true"></i></a> ';
	info += '<i class="fa fa-15x fa-code btn-embed" aria-hidden="true"></i> ';
	info += '<i class="fa fa-15x fa-twitter btn-twitter" aria-hidden="true"></i></div>';

	var src = "https://player.xrcb.cat/?url="+encodeURIComponent(btn.data("src"));

	if (btn.data("radio") != "") {
		src += "&radio="+encodeURIComponent(btn.data("radio"));
	}
	if (btn.data("programa") && btn.data("programa") != "") {
		src += "&programa="+encodeURIComponent(btn.data("programa"));
	}
	if (btn.data("title") != "") {
		src += "&podcast="+encodeURIComponent(btn.data("title"));
	}
	if (btn.data("podcast-link") != "") {
		src += "&podcastlink="+encodeURIComponent(btn.data("podcast-link"));
	}
	if (btn.data("radio-link") != "")
	src += "&radiolink"+encodeURIComponent(btn.data("radio-link"));

	info += "<div class='embed_code'><pre>&lt;iframe width='600' height='60' src='"+src+"' frameborder='0' scrolling='no' style='overflow: hidden; max-width: 100%;'&gt;Iframe no esta soportado en tu navegador.&lt;/iframe&gt;</pre></div>";

	jQuery(".xrcbplayer .moreinfo-content").empty();
	jQuery(".xrcbplayer .moreinfo-content").append(info);

	jQuery(".moreinfo a").attr("href", btn.data("podcast-link"));

	bindInfoBtns();
}

function bindInfoBtns() {
	jQuery(".moreinfo-content .btn-embed").unbind();
	jQuery(".moreinfo-content .btn-embed").click(function(){
		// open info window
		jQuery(".moreinfo-content .embed_code").toggle();
	});

	jQuery(".moreinfo-content .btn-twitter").unbind();
	jQuery(".moreinfo-content .btn-twitter").click(function(){
		createTwitterMsg();
	});
}

function shortenStr(preStr, str) {
	if ((preStr.length+str.length) > 67) {
		str = str.substring(0,67-preStr.length) + "...";
	}
	return str;
}

function createTwitterMsg(radio=null, podcast=null, podcastlink=null) {
	var msg = "Estoy escuchando la emissión de @xrcbcn en https://xrcb.cat/ #XRCB";

	if (player.getSrc() && player.getSrc() !== broadcastUrl && player.getSrc() !== liveUrl) {
		radio = jQuery(".xrcbplayer .radio").text();
		podcast = jQuery(".xrcbplayer .podcast").text();
		podcastlink = jQuery(".xrcbplayer .podcast-link").text();
	}
	else if (player.getSrc() === broadcastUrl || player.getSrc() === liveUrl) {
		podcast = jQuery(".xrcbplayer .live").text();
		msg = "Escuchando la emisión en directo de "+podcast+" en la @xrcbcn #XRCB https://xrcb.cat/ca/event/radioton-02-radio-cava-ret/";
	}

	if (radio && podcast && podcastlink) {
		msg = "Estoy escuchando el podcast "+jQuery(this).data("title")+ " de "+radio+" en la @xrcbcn #XRCB "+podcastlink;
	}

	var conf = "height=300,width=600,left=200,top=200,menubar=no,location=no,resizable=yes,scrollbars=no,status=no";
	window.open("https://twitter.com/intent/tweet?text=" + encodeURIComponent(msg), "XRCB Twitter", conf);
}

function playBroadcast(live=false) {
	var streamUrl = broadcastUrl;
	if (live) {
		streamUrl = liveUrl;
		//jQuery(".xrcbplayer .player").removeClass("src-podcast");// remove class playing podcast
		//jQuery(".xrcbplayer .player").addClass("src-stream");// set class playing stream
	}
	jQuery(".broadcast-btn").addClass("live");

	if (player.getSrc() !== streamUrl) {
		xrcbplay(streamUrl);

		//if (!live) {
			// broadcast
			jQuery("#toggle-live").prop('checked', true);
			//jQuery(".xrcbplayer .player").removeClass("src-podcast");// remove class playing podcast
			//jQuery(".xrcbplayer .player").addClass("src-stream");// set class playing stream

			// mobile bg player state
			jQuery("#mobileplayer .play").addClass("pause");
			jQuery(".xrcbplayer .podcast").text("");
			jQuery(".xrcbplayer #live").text("");

			setStreamTitle();
			setStreamInfo();
		/*}
		else {
			jQuery(".xrcbplayer #live").text(" CitySound. La construcció sonora de la ciutat");
		}*/

		jQuery(".btn-play").removeClass("active");

		jQuery(".xrcbplayer .radio").text("");
		jQuery(".xrcbplayer #live").addClass("live");
		jQuery(".xrcbplayer .sep").css("display", "none");
	}
	player.play();
}

window.mobilecheck = function() {
	var check = false;
	(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
	return check;
};
