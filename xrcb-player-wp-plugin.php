<?php
/*
Plugin Name: XRCB Player
Plugin URI: https://gitlab.com/guifi-exo/xrcb/xrcb-player-wp-plugin
Description: Custom podcast and mp3 player for XRCB website
Version: 2.0
Requires: 5.0
Requires PHP: 7.0
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html
Author: Gerald Kogler
Author URI: http://go.yuri.at
Text Domain: xrcbplayer
*/

/**
* load textdomain
*/
add_action( 'init', 'xrcbplayer_load_textdomain' );
function xrcbplayer_load_textdomain() {
	load_plugin_textdomain('xrcbplayer', false, dirname(plugin_basename(__FILE__)));
}

/**
* remove WP media player styles
*/
function xrcb_deregister_media_element_styles() {
	wp_deregister_style( 'mediaelement' );
	wp_deregister_style( 'wp-mediaelement' );
}
add_action( 'wp_enqueue_scripts', 'xrcb_deregister_media_element_styles' );

/**
* Add own custom CSS and JS file to reskin the audio player
* any kind of assets will be declared here
*/
function xrcb_add_audio_player_assets () {
	wp_enqueue_style('xrcb-audio-player', plugin_dir_url( __FILE__ ) . 'assets/css/xrcbplayer.css', array(), time() );
	wp_enqueue_script( 'xrcb-audio-player', plugin_dir_url( __FILE__ ) . 'assets/js/xrcbplayer.js', array(), '20180910', true );
}
add_action( 'wp_enqueue_scripts', 'xrcb_add_audio_player_assets');

/**
* overwrite mp3 shortcode with custom player code
*/
function xrcb_overwrite_audio_shortcode() {

	function audio_xrcbplayer( $atts ) {
		// no default values
		$audio = shortcode_atts( array(
			'mp3' => 'nothing',
			'radio' => '',
			'programa' => '',
			'title' => '',
			'radio-link' => '',
			'podcast-link' => '',
			'label' => '',
		), $atts );

		$label = "";
		if ($audio['label'] !== "") {
			$label = "<div class='btn-play-label'>{$audio['label']}</div>";
		}

		return "<div class='btn-play-container'><div class='btn btn-play btn-play-inverse piwik_download' data-src='{$audio['mp3']}' data-radio='{$audio['radio']}' data-label='{$audio['label']}' data-programa='{$audio['programa']}' data-title='{$audio['title']}' data-radio-link='{$audio['radio-link']}' data-podcast-link='{$audio['podcast-link']}'></div></div>".$label;
	}
	remove_shortcode('audio');
	add_shortcode( 'audio', 'audio_xrcbplayer' );
}
add_action( 'wp_loaded', 'xrcb_overwrite_audio_shortcode' );

/**
* insert player template
*/
function xrcbplayer_show_player() {
	require_once( plugin_dir_path( __FILE__ ) . 'templates/player.php' );
}

?>
